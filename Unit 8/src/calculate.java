import java.io.*;
import java.util.*;

public class calculate {

	public static void main(String[] args) throws IOException {

		if (args.length != 1) {
			System.err.println("Usage: calculate <file1>");
			System.exit(1);
		}
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(args[0]));
			int numFields = 0;
			String s = null;
			Scanner s2 = null;
			Scanner s3 = null;
			String[] names = null;
			s = in.readLine();
			try {
				s2 = new Scanner(s);
				s2.useDelimiter(",");
				while (s2.hasNext()) {
					numFields++;
					s2.next();
				}
				names = new String[numFields];
				s3 = new Scanner(s);
				s3.useDelimiter(",");
				for (int i = 0; i < names.length; i++) {
					names[i] = s3.next();
				}
			} finally {
				s2.close();
				s3.close();
			}
			int[] fields = new int[numFields];
			for (int i = 0; i < fields.length; i++) {
				fields[i] = 0;
			}
			while ((s = in.readLine()) != null) {
				try {
					s2 = new Scanner(s);
					s2.useDelimiter(",");
					for (int i = 0; i < fields.length; i++) {
						fields[i] += s2.nextInt();
					}
				} finally {
					s2.close();
				}
			}
			for (int i = 0; i < fields.length; i++) {
				System.out.println(names[i] + ": " + fields[i]);
			}

		} finally {
			if (in != null) {
				in.close();
			}
		}

	}

}
