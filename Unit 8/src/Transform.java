import java.io.*;

public class Transform {

	public static void main(String[] args) throws IOException {

		if (args.length != 2) {
			System.err.println("Usage: Transform <file1> <file2>");
			System.exit(1);
		}

		BufferedReader input = null;
		PrintWriter output = null;

		try {
			input = new BufferedReader(new FileReader(args[0]));
			output = new PrintWriter(new FileWriter(args[1]));
			
			String line;
			
			while((line = input.readLine()) != null) {
				output.println(line.toUpperCase());
			}
		}catch (FileNotFoundException ex) {
			System.err.println("File "+args[0]+" not found.");
		}catch(IOException ex) {
			System.err.println(ex.getLocalizedMessage());
		} finally {
			if (input != null) {
				input.close();
			}
			if (output != null) {
				output.close();
			}
		}
	}

}
