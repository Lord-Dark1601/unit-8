import java.io.*;

public class CountLines {

	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.err.println("Usage: CountLines <file>");
			System.exit(1);
		}
		BufferedReader input = null;
		int count = 0;

		try {
			input = new BufferedReader(new FileReader(args[0]));

			while (input.readLine() != null) {
				count++;
			}
		} catch (FileNotFoundException ex) {
			System.err.println("File " + args[0] + " not found.");
		} catch (IOException ex) {
			System.err.println(ex.getLocalizedMessage());
		} finally {
			if (input != null) {
				input.close();
			}
			System.out.println("The number of lines are: " + count);
		}
	}
}
