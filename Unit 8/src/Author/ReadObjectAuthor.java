package Author;

import java.io.*;

public class ReadObjectAuthor {

	public static void main(String[] args) throws IOException {
		ObjectInputStream input = null;

		try {
			input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(StoreObjecAuthor.FILE_NAME)));

			try {
				while (true) {
					Author a = (Author) input.readObject();
				}
			} catch (ClassNotFoundException ex) {
				System.out.println();
			}
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

}