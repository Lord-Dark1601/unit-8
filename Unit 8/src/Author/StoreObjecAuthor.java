package Author;

import java.io.*;
import java.util.*;

public class StoreObjecAuthor {

	public static final String FILE_NAME = "Authors.obj";

	public static void main(String[] args) throws IOException {

		ObjectOutputStream out = null;
		Scanner input = new Scanner(System.in);

		try {
			out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(FILE_NAME)));

			Author a = Author.createAuthorFromKeyboard(input);
			Author b = Author.createAuthorFromKeyboard(input);

			out.writeObject(a);
			out.writeObject(b);

		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

}
