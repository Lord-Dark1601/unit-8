package Author;

import java.io.*;
import java.util.*;

public class DataStreamAuthors {

	private static String dataFile = "authorsFile";

	public static void main(String[] args) throws IOException{

		writeAuthor();
		readAuthor();
	}

	public static void writeAuthor() throws IOException {
		Scanner input = new Scanner(System.in);
		Author author = Author.createAuthorFromKeyboard(input);
		input.close();
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)));

			out.writeUTF(author.getName());
			out.writeUTF(author.getEmail());
			out.writeChar(author.getGender());

		} finally {
			if (out != null) {
				out.close();
			}
		}
	}
	
	public static void readAuthor() throws IOException {
		DataInputStream input = null;
		try {
			input = new DataInputStream(new BufferedInputStream(new FileInputStream(dataFile)));
			try {
				while(true) {
					String name = input.readUTF();
					String email = input.readUTF();
					char gender = input.readChar();
					
					System.out.print("Author: "+name);
					System.out.print("("+gender+"), ");
					System.out.print("with Email: "+email);
					System.out.println();
					
				}
			}catch (IOException ex) {
				System.out.println();
			}
		}finally {
			if (input != null) {
				input.close();
			}
		}
	}

}
