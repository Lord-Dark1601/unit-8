package Author;

import java.io.*;
import java.util.*;

public class StoreObjectBook {

	public static final String FILE_NAME = "Books.obj";

	public static void main(String[] args) throws IOException {

		ObjectOutputStream out = null;
		Scanner input = new Scanner(System.in);

		try {
			out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(FILE_NAME)));

			Book a = Book.createBookFromKeyboard(input);
			Book b = Book.createBookFromKeyboard(input);

			out.writeObject(a);
			out.writeObject(b);

		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

}
