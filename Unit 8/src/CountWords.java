import java.io.*;
import java.util.*;

public class CountWords {

	public static void main(String[] args) throws IOException {

		if (args.length < 1) {
			System.err.println("Usage: CountWords <file1>...<fileN>");
			System.exit(1);
		}

		Scanner s = null;

		try {
			int count = 0;
			for (int i = 0; i < args.length; i++) {
				s = new Scanner(new BufferedReader(new FileReader(args[i])));

				while (s.hasNext()) {
					count++;
					s.next();
				}
			}
			System.out.println("The number of words is: " + count);

		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

}
